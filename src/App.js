import { useState, useEffect } from 'react';
import {
    fraction,
    multiply, add, subtract,
    smallerEq, smaller,
    larger, largerEq,
    number, equal, divide,
    isInteger, floor, abs,
} from 'mathjs';

console.log(getFractionalPart(fraction('-3/10')));

function getFractionalPart(fr) {
    return subtract(0, subtract(fr, floor(fr)));
}

function fractionString(fr) {
    const f = fraction(fr)
    return `${f.s < 0 ? '-' : ''}${f.n}${f.d === 1 ? '' : `/${f.d}`}`;
};

function arrayCyclicShift(arr, toRight) {
    if (!toRight) arr.unshift(arr.pop());
    else arr.push(arr.shift());
    return arr;
}

function App() {
    const [variablesCount, setVariablesCount] = useState(3);
    const [constraintsCount, setConstraintsCount] = useState(3);

    const [tables, setTables] = useState([]);
    const [result, setResult] = useState();

    const [objectiveFunction, setObjectiveFunction] = useState(
        [1, 1, 1]
    );
    const [constraints, setConstraints] = useState([
        [0.3, 0.1, 0.2, 50],
        [0.5, 0.2, 0.4, 60],
        [0.4, 0.5, 0.3, 40],
    ]);

    const [initialRender, setInitialRender] = useState(true);
    useEffect(() => {
        if (initialRender) {
            setInitialRender(false);
        } else {
            setObjectiveFunction(Array(variablesCount).fill(0));
            setConstraints(Array(constraintsCount).fill(0).map(
                _ => Array(variablesCount + 1).fill(0),
            ));
        }
    }, [variablesCount, constraintsCount]);

    const handleCountChange = (set, value, limit) => {
        let result = Number(value);
        if (result <= 1)
            result = 1;
        else if (limit && result >= limit)
            result = limit;

        set(Math.floor(result));
    };

    const handleSubmit = e => {
        e.preventDefault();
        setTables([]);

        const simplexTable = {
            colB: Array.from({ length: constraintsCount }, (_, i) => i + variablesCount + 1),
            rowC: [
                fraction(0),
                ...objectiveFunction.map(c => fraction(c)),
                ...Array(constraintsCount).fill(0).map(e => fraction(e)),
            ],
            rows: constraints
                .map(constraint => arrayCyclicShift(constraint.map(c => fraction(c))))
                .map(
                    (constraint, constraintIndex) => [
                        ...constraint,
                        ...Array(constraintsCount)
                            .fill(0)
                            .map((_, index) => fraction(constraintIndex === index ? 1 : 0)),
                    ],
                ),
            recalculateRowD: function() {
                this.rowD = this.rowC.map((C, Cindex) => {
                    let D = fraction(-C);

                    this.colB.map((B, Bindex) => {
                        D = add(D, multiply(this.rowC[B], this.rows[Bindex][Cindex]));
                    });

                    return D;
                });
            },
            analyze: function () {    
                this.recalculateRowD();

                const [maxD, ...restD] = this.rowD;

                if (this.rowD.find((D, Dindex) => smaller(D, 0) && this.rows.every(row => smallerEq(row[Dindex], 0))))
                    return 'Objective function is not limited on feasible region.';

                if (
                    this.isGomori
                        ? this.rows.every(row => largerEq(row[0], 0))
                        : restD.every(D => largerEq(D, 0))
                ) {
                    const result = this.colB.map(
                        (B, Bindex) => B <= variablesCount ? [B, this.rows[Bindex][0]] : [,]
                    ).filter(B => B[0]);

                    const fractionInResult = result.find(([varNum, value]) => value && !isInteger(value))
                    if (fractionInResult) {
                        this.isGomori = true;
                        tables.push((({ guideRow, guideCol, ...rest }) => rest)(JSON.parse(JSON.stringify(simplexTable))));
                        this.rows.push(
                            this.rows[this.colB.findIndex((B, Bindex) => B === fractionInResult[0])]
                                .map(element => getFractionalPart(element)),
                        );
                        this.rows.map(
                            (row, index) => {
                                if (index + 1 === this.rows.length) {
                                    row.push(fraction(1));
                                    this.rowC.push(fraction(0));
                                    this.colB.push(row.length - 1);
                                } else {
                                    row.push(fraction(0));
                                }
                            }
                        );
                        this.recalculateRowD();
                    } else {
                        return `F(max) = ${fractionString(maxD)}${result.map(
                            ([variableNum, value]) => `, x${variableNum} = ${value}`
                        ).join('')}.`;
                    }
                }
            },
            calculateGuideElement: function () {
                const [maxD, ...restD] = this.rowD;

                const min = fraction(Math.min(...restD.map(D => number(D))));
                this.guideCol = restD.findIndex(
                    D => equal(D, min)
                ) + 1;

                const guideRowMin = Math.min(
                    ...this.rows
                        .filter(row => larger(row[this.guideCol], 0))
                        .map(row => number(divide(row[0], row[this.guideCol])))
                );

                this.guideRow = this.rows.findIndex(
                    row => larger(row[this.guideCol], 0) && 
                        number(divide(row[0], row[this.guideCol])) === guideRowMin
                );
            },
            calculateGomoriGuideElement: function () {
                const A0 = this.rows.map(row => row[0]);

                const min = fraction(Math.min(...A0.map(D => number(D))));
                const guideRowIndex = A0.findIndex(A0i => equal(A0i, min));
                this.guideRow = guideRowIndex;

                const [_, ...guideRowElements] = this.rows[guideRowIndex];
                const guideColMin = Math.min(
                    ...guideRowElements
                        .map(
                            (el, index) => this.rowD[index + 1].n !== 0 &&
                                el.n !== 0 && 
                                number(abs(divide(this.rowD[index + 1], el)))
                        )
                        .filter(el => el !== false)
                );

                this.guideCol = guideRowElements.findIndex(
                    (el, index) => this.rowD[index + 1].n !== 0 && 
                        el.n !== 0 && 
                        number(abs(divide(this.rowD[index + 1], el))) === guideColMin
                ) + 1;
            },
            recalculate: function () {
                this.isGomori
                    ? this.calculateGomoriGuideElement()
                    : this.calculateGuideElement();

                this.colB[this.guideRow] = this.guideCol;
                this.rows = this.rows.map((row, rowIndex) => {
                    if (rowIndex === this.guideRow) {
                        return row.map(el => divide(el, this.rows[this.guideRow][this.guideCol]));
                    } else {
                        return row.map(
                            (el, colIndex) => subtract(
                                el,
                                divide(
                                    multiply(
                                        this.rows[this.guideRow][colIndex],
                                        this.rows[rowIndex][this.guideCol]
                                    ),
                                    this.rows[this.guideRow][this.guideCol]
                                )
                            )
                        );
                    }
                });
            },
        };

        let tables = []

        while (true) {
            const result = simplexTable.analyze();
            tables.push(JSON.parse(JSON.stringify(simplexTable)));
            console.log({ simplexTable: JSON.parse(JSON.stringify(simplexTable)) });

            if (result) {
                console.log({ result });
                setResult(result);
                break;
            }

            simplexTable.recalculate();
        }

        setTables(tables);
    };

    return (
        <div className="App m-5">
            <form onSubmit={handleSubmit}>
                <h2 className='mb-5'>Variables count</h2>
                <input
                    type="number"
                    step="1"
                    min="1"
                    max="9"
                    value={variablesCount}
                    onChange={e => handleCountChange(setVariablesCount, e.target.value, 9)}
                    className={
                        `bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg 
                        focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700
                        dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 
                        dark:focus:border-blue-500 mb-5`
                    }
                    required
                />
                <h2 className='mb-5'>Constraints count</h2>
                <input
                    type="number"
                    step="1"
                    min="1"
                    max="20"
                    value={constraintsCount}
                    onChange={e => handleCountChange(setConstraintsCount, e.target.value, 20)}
                    className={
                        `bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg 
                        focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 
                        dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 
                        dark:focus:border-blue-500 mb-5`
                    }
                    required
                />
                <h2 className='mb-5'>Objective function</h2>
                <div className={`grid gap-6 md:grid-cols-10`}>
                    {objectiveFunction.map((variable, variableIndex) => (
                        <div className="flex items-center gap-6 mb-6">
                            <input
                                type="number"
                                value={variable}
                                onChange={e => setObjectiveFunction(objectiveFunction => {
                                    const copy = JSON.parse(JSON.stringify(objectiveFunction));
                                    copy[variableIndex] = e.target.value;
                                    return copy;
                                })}
                                className={
                                    `bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg 
                                    focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 
                                    dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 
                                    dark:focus:border-blue-500 w-full`
                                }
                                required
                            />
                            <div className='whitespace-nowrap'>
                                x{variableIndex + 1} {variableIndex + 1 === objectiveFunction.length ? '' : '+'}
                            </div>
                        </div>
                    ))}
                    <div className="flex items-center gap-6 mb-6">{'-> Max'}</div>
                </div>
                <h2 className='mb-5'>Constraints</h2>
                {constraints.map((constraint, constraintIndex) => (
                    <div className={`grid gap-6 md:grid-cols-10`}>
                        {constraint.map((variable, variableIndex) => (
                            <div className="flex items-center gap-6 mb-6">
                                <input
                                    type="number"
                                    value={variable}
                                    {...((variableIndex + 1 === constraint.length) ? { min: 0 } : {})}
                                    onChange={e => setConstraints(constraints => {
                                        const copy = JSON.parse(JSON.stringify(constraints));
                                        copy[constraintIndex][variableIndex] = e.target.value;
                                        return copy;
                                    })}
                                    className={
                                        `bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg 
                                        focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 
                                        dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 
                                        dark:focus:border-blue-500 w-full`
                                    }
                                    required
                                />
                                <div className='whitespace-nowrap'>
                                    {variableIndex + 1 === constraint.length
                                        ? ''
                                        : `x${variableIndex + 1} ${variableIndex + 2 === constraint.length ? '<=' : '+'}`}
                                </div>
                            </div>
                        ))}
                    </div>
                ))}
                <button
                    type="submit"
                    className={
                        `text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none 
                        focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 
                        text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800`
                    }
                >
                    Calculate
                </button>
            </form>

            {tables.map((table, tableIndex) => (
                <div className="relative overflow-x-auto sm:rounded-lg my-5 border border-gray-300">
                    <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                        <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                            <tr>
                                <th scope="col" className="px-6 py-3">C</th>
                                {table.rowC.map((C, Cindex) => (
                                    <th scope="col" className="px-6 py-3">
                                        {Cindex === 0 ? '-' : fractionString(C)}
                                    </th>
                                ))}
                            </tr>
                            <tr>
                                <th scope="col" className="px-6 py-3">B</th>
                                {table.rowC.map((C, Cindex) => (
                                    <th scope="col" className="px-6 py-3">
                                        A{Cindex}
                                    </th>
                                ))}
                            </tr>
                        </thead>
                        <tbody>
                            {table.rows.map((row, rowIndex) => (
                                <tr className="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                                    <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                        x{table.colB[rowIndex]}
                                    </th>
                                    {row.map((element, colIndex) => (
                                        <td className={
                                            `px-6 py-4 
                                            ${tables.length !== tableIndex + 1 &&
                                                tables[tableIndex + 1].guideRow === rowIndex &&
                                                tables[tableIndex + 1].guideCol === colIndex
                                                ? 'text-blue-700 font-bold'
                                                : ''
                                            }`
                                        }>
                                            {fractionString(element)}
                                        </td>
                                    ))}
                                </tr>
                            ))}
                            <tr className="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                                <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                    Δ
                                </th>
                                {table.rowD.map(element => (
                                    <td className="px-6 py-4">
                                        {fractionString(element)}
                                    </td>
                                ))}
                            </tr>
                        </tbody>
                    </table>
                </div>
            ))}
            {result && <h2 className='mb-5'>{result}</h2>}

        </div>
    );
}

export default App;
